From 9693d3707e16f783b97dcf0a7340ded0427aa3c0 Mon Sep 17 00:00:00 2001
From: Soshun Naito <soshun@google.com>
Date: Wed, 7 Aug 2024 03:47:23 +0000
Subject: [PATCH] Mojo: Introduce [Uuid] attribute

This is a cherry-pick of the CL: https://crrev.com/c/2462378
It adds Uuid_ attribute to the corresponding cpp interface when we add
[Uuid=<UUID>] to the mojo interface.

Bug: b:357737923, b:40152372
Test: m libmojo
Change-Id: I927361da96eba66420f6c95777cba43b0055baec
---
 mojo/public/tools/bindings/README.md            |  7 +++++++
 .../cpp_templates/interface_declaration.tmpl    |  4 ++++
 .../cpp_templates/interface_definition.tmpl     |  3 +++
 .../bindings/generators/mojom_cpp_generator.py  |  5 ++++-
 .../bindings/pylib/mojom/generate/module.py     | 17 +++++++++++++++++
 5 files changed, 35 insertions(+), 1 deletion(-)

diff --git a/mojo/public/tools/bindings/README.md b/mojo/public/tools/bindings/README.md
index d1ffc448e..ce291ae0e 100644
--- a/mojo/public/tools/bindings/README.md
+++ b/mojo/public/tools/bindings/README.md
@@ -395,6 +395,13 @@ interesting attributes supported today.
     field, enum value, interface method, or method parameter was introduced.
     See [Versioning](#Versioning) for more details.
 
+**`[Uuid=<UUID>]`**
+:  Specifies a UUID to be associated with a given interface. The UUID is
+   intended to remain stable across all changes to the interface definition,
+   including name changes. The value given for this attribute should be a
+   standard UUID string representation as specified by RFC 4122. New UUIDs can
+   be generated with common tools such as `uuidgen`.
+
 **`[EnableIf=value]`**
 :   The `EnableIf` attribute is used to conditionally enable definitions when
     the mojom is parsed. If the `mojom` target in the GN file does not include
diff --git a/mojo/public/tools/bindings/generators/cpp_templates/interface_declaration.tmpl b/mojo/public/tools/bindings/generators/cpp_templates/interface_declaration.tmpl
index 193d380e7..bd007ab2a 100644
--- a/mojo/public/tools/bindings/generators/cpp_templates/interface_declaration.tmpl
+++ b/mojo/public/tools/bindings/generators/cpp_templates/interface_declaration.tmpl
@@ -13,6 +13,10 @@ class {{export_attribute}} {{interface.name}}
     : public {{interface.name}}InterfaceBase {
  public:
   static const char Name_[];
+{%-  if interface.uuid %}
+  static constexpr base::Token Uuid_{ {{interface.uuid[0]}}ULL,
+                                      {{interface.uuid[1]}}ULL };
+{%-  endif %}
   static constexpr uint32_t Version_ = {{interface.version}};
   static constexpr bool PassesAssociatedKinds_ = {% if interface|passes_associated_kinds %}true{% else %}false{% endif %};
   static constexpr bool HasSyncMethods_ = {% if interface|has_sync_methods %}true{% else %}false{% endif %};
diff --git a/mojo/public/tools/bindings/generators/cpp_templates/interface_definition.tmpl b/mojo/public/tools/bindings/generators/cpp_templates/interface_definition.tmpl
index 72c3101c1..bc3200bf6 100644
--- a/mojo/public/tools/bindings/generators/cpp_templates/interface_definition.tmpl
+++ b/mojo/public/tools/bindings/generators/cpp_templates/interface_definition.tmpl
@@ -32,6 +32,9 @@ std::move(p_{{param.name}})
 
 {#--- Begin #}
 const char {{class_name}}::Name_[] = "{{namespace}}.{{class_name}}";
+{%-  if interface.uuid %}
+constexpr base::Token {{class_name}}::Uuid_;
+{%-  endif %}
 
 {#--- Constants #}
 {%-  for constant in interface.constants %}
diff --git a/mojo/public/tools/bindings/generators/mojom_cpp_generator.py b/mojo/public/tools/bindings/generators/mojom_cpp_generator.py
index 97bc827c9..b6519a80b 100644
--- a/mojo/public/tools/bindings/generators/mojom_cpp_generator.py
+++ b/mojo/public/tools/bindings/generators/mojom_cpp_generator.py
@@ -256,16 +256,19 @@ class Generator(generator.Generator):
     return used_typemaps
 
   def _GetExtraPublicHeaders(self):
+    headers = set()
+
     all_enums = list(self.module.enums)
     for struct in self.module.structs:
       all_enums.extend(struct.enums)
     for interface in self.module.interfaces:
       all_enums.extend(interface.enums)
+      if interface.uuid:
+        headers.add('base/token.h')
 
     types = set(self._GetFullMojomNameForKind(typename)
                 for typename in
                 self.module.structs + all_enums + self.module.unions)
-    headers = set()
     for typename, typemap in self.typemap.items():
       if typename in types:
         headers.update(typemap.get("public_headers", []))
diff --git a/mojo/public/tools/bindings/pylib/mojom/generate/module.py b/mojo/public/tools/bindings/pylib/mojom/generate/module.py
index aeeb4fce0..6a48791e5 100644
--- a/mojo/public/tools/bindings/pylib/mojom/generate/module.py
+++ b/mojo/public/tools/bindings/pylib/mojom/generate/module.py
@@ -12,6 +12,8 @@
 # method = interface.AddMethod('Tat', 0)
 # method.AddParameter('baz', 0, mojom.INT32)
 
+from uuid import UUID
+
 # We use our own version of __repr__ when displaying the AST, as the
 # AST currently doesn't capture which nodes are reference (e.g. to
 # types) and which nodes are definitions. This allows us to e.g. print
@@ -224,6 +226,7 @@ PRIMITIVES = (
 ATTRIBUTE_MIN_VERSION = 'MinVersion'
 ATTRIBUTE_EXTENSIBLE = 'Extensible'
 ATTRIBUTE_SYNC = 'Sync'
+ATTRIBUTE_UUID = 'Uuid'
 
 
 class NamedValue(object):
@@ -642,6 +645,20 @@ class Interface(ReferenceKind):
     for constant in self.constants:
       constant.Stylize(stylizer)
 
+  @property
+  def uuid(self):
+    uuid_str = self.attributes.get(ATTRIBUTE_UUID) if self.attributes else None
+    if uuid_str is None:
+      return None
+
+    try:
+      u = UUID(uuid_str)
+    except:
+      raise ValueError('Invalid format for Uuid attribute on interface {}. '
+                       'Expected standard RFC 4122 string representation of '
+                       'a UUID.'.format(self.mojom_name))
+    return (int(u.hex[:16], 16), int(u.hex[16:], 16))
+
 
 class AssociatedInterface(ReferenceKind):
   ReferenceKind.AddSharedProperty('kind')
-- 
2.46.0.rc2.264.g509ed76dc8-goog

